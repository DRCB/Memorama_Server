from .models import Carta
from rest_framework import serializers

class CartaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Carta
        fields = ('id','imagen','tapada')