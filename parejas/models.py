# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Carta (models.Model):
    imagen = models.TextField()
    tapada = models.BooleanField(default = True)
